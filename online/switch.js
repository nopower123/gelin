window.gl_switch = function(lib, game, ui, get, ai, _status) {
	lib.gl_switch = {
		list: ['chunnuanhuakai'],
		translate: {
			'chunnuanhuakai': '春暖花开',
		},
		chunnuanhuakai: {
			init: function() {
				galgame.sce('chunnuanhuakai1');
				game.boss.noRemove = true;
			},
			cmd: false,
			player: 'gl_aileina',
			galgame: {
				"chunnuanhuakai1": [
					'gl_ailixiya:今天天气真不错呢~',
					'gl_dimu:可惜你要在教室里上课。',
					'gl_ailixiya:嘁，真扫兴！',
					'gl_kali:小家伙们，在讨论什么呢？',
					'gl_dimu:艾莉西娅说她要罢课。',
					'gl_ailixiya:喂喂喂，我可不是这么说的！我只是感慨一下天气不错而已啊！',
					'gl_kali:哈哈哈，今天天气的确不错，干脆停课去春游吧~',
					'gl_ailixiya:真的假的？！',
					'gl_kali:要劳逸结合嘛~而且......',
					'none:卡莉看向艾蕾娜。',
					'gl_ailixiya:艾蕾娜酱~在想什么呢？',
					'gl_aileina:啊？哦！今天的课程是什么？'
				],
				'chunnuanhuakai2': [
					'gl_aileina:春天啊......我记得好像有个叫“希望与亲爱之春”的想区。那里的风景会有所不同吗？',
					'hundun_wumianliming:奇怪......这个地方明明也是说书人的力量创造的，却没有引导“故事”的诗晶石存在......',
					'gl_aileina:呀？欢迎来到福音学院，请问您找谁？',
					'hundun_wumianliming:嗯？原住民？不太像......不管了，抓回去再说！'
				],
				'chunnuanhuakai3': [
					'gl_aileina:啊啊啊啊啊————',
					'gl_dimu:麻烦。',
					'none:蒂姆一个闪身接住了艾蕾娜。',
					'gl_aileina:对...对不起......',
					'gl_ailixiya:你这家伙，竟敢欺负我的艾蕾娜酱？！我要打扁你哦！',
					'hundun_wumianliming:真麻烦......'
				],
				'chunnuanhuakai4': [
					'gl_aileina:对手太强了！',
					'gl_ailixiya:咳咳咳，被诗音老师揍的时候也没这么惨啊......',
					'gl_dimu:那是因为诗音老师每次都放水了，笨蛋。',
					'gl_ailixiya:你才是笨蛋！',
					'hundun_wumianliming:够了！叽叽歪歪的当我不存在吗？！',
					'gl_kali:哼！敢弄伤他们当我不存在吗？！',
					'none:大量的迷雾不知从何处涌出，直接覆盖了整个战场。',
					'hundun_wumianliming:这...这是？！'
				],
				'chunnuanhuakai5': [
					'gl_kali:抱歉孩子们，我也没料到会发生这种事......',
					'gl_aileina:没关系的校长大人~',
					'gl_kali:我得回去研究一下，今天的春游只能就此结束了......',
					'gl_ailixiya:啊？也没产生什么不可挽回的后果啊！',
					'gl_dimu:等产生不可挽回的后果就太迟了。',
					'gl_kali:抱歉，那就用这个作为补偿......',
					'gl_ailixiya:别别别，我就随口抱怨一下......',
					'gl_kali:没关系~你们收下我也能轻松点~',
					'gl_aileina:那就谢谢校长啦~'
				]
			},
			pack: {
				character: {
					"hundun_wumianliming": ["male", "jin", 4, ['gl_hundun', 'hundun_shunzhan', 'hundun_huxiao', 'hundun_mingqi'],
						[]
					],
					"gl_aileina": ["female", "key", 3, ["biyue", "reqingguo"],
						["ext:格林笔记/gl_aileina.jpg"]
					],
					"gl_dimu": ["male", "qun", 4, ["gl_zishou", "gl_guicai", "gl_nishi"],
						["ext:格林笔记/gl_dimu.jpg"]
					],
					"gl_ailixiya": ["female", "key", 4, ["gl_haoqi", "gl_caishi", "gl_futu"],
						["ext:格林笔记/gl_ailixiya.jpg"]
					],
				},
				skill: {
					_open: {
						trigger: {
							global: "phaseBefore",
						},
						filter: function(event, player) {
							if (game.xiangqv.contains('open')) return false;
							return true;
						},
						forced: true,
						lastDo: true,
						forceDie: true,
						content: function() {
							game.xiangqv.push('open');
							galgame.sce('chunnuanhuakai2');
						},
					},
					_win1: {
						trigger: {
							player: "dieBefore",
						},
						filter: function(event, player) {
							if (game.xiangqv.contains('win1')) return false;
							if (player.identity != 'nei') return false;
							var bool = game.hasPlayer(function(current) {
								return current.identity == 'nei' && current != player;
							})
							return !bool;
						},
						lastDo: true,
						forceDie: true,
						forced: true,
						content: function() {
							'step 0'
							trigger.cancel();
							player.recover(player.maxHp - player.hp);
							galgame.sce('chunnuanhuakai3');
							'step 1'
							var data = [{
								name: "gl_ailixiya",
								position: 1,
								identity: "nei"
							}, {
								name: "gl_dimu",
								position: 7,
								identity: "nei"
							}]
							game.nextLevel(data);
							game.gameDraw(game.players[game.players.length], 4);
							game.arrangePlayers();
							game.xiangqv.push('win1');
						}
					},
					_win2: {
						trigger: {
							player: "dieBefore",
						},
						filter: function(event, player) {
							if (!game.xiangqv.contains('win1')) return false;
							if (game.xiangqv.contains('win2')) return false;
							if (player.identity != 'nei') return false;
							var bool = game.hasPlayer(function(current) {
								return current.identity == 'nei' && current != player;
							})
							return !bool;
						},
						lastDo: true,
						forceDie: true,
						forced: true,
						content: function() {
							'step 0'
							galgame.sce('chunnuanhuakai4');
							'step 1'
							var data = [{
								name: "gl_kali",
								position: 0,
								init: function(player) {
									game.vitalPlayer.me = player;
								},
								identity: "nei"
							}]
							delete game.vitalPlayer.me.noRemove;
							if (game.me) {
								cards = game.me.getCards('hejsx');
								for (var j = 0; j < cards.length; j++) {
									cards[j].discard();
								}
								game.removePlayer(game.me);
							}
							game.nextLevel(data);
							game.swapControl(game.vitalPlayer.me);
							event.player = game.vitalPlayer.me;
							game.boss.dataset.position = 4;
							game.gameDraw(game.players[game.players.length], 4);
							game.arrangePlayers();
							game.xiangqv.push('win2');
							'step 2'
							var cards = Array.from(ui.ordering.childNodes);
							while (cards.length) {
								cards.shift().discard();
							}
							'step 3'
							while (_status.event.name != 'phaseLoop') {
								_status.event = _status.event.parent;
							}
							game.resetSkills();
							_status.paused = false;
							_status.event.player = game.vitalPlayer.me;
							_status.event.step = 0;
						}
					},
					_win3: {
						trigger: {
							player: "dieAfter",
						},
						filter: function(event, player) {
							return player == game.boss;
						},
						lastDo: true,
						forceDie: true,
						forced: true,
						content: function() {
							'step 0'
							galgame.sce('chunnuanhuakai5');
							if (!game.xiangqv.contains('win1')) {
								game.gl_gain('gl_kelalisi');
							} else if (!game.xiangqv.contains('win2') && Math.random() < 0.1) {
								game.gl_gain('gl_kelalisi');
							}
							if (!lib.config.gelin.count) lib.config.gelin.count = 0;
							lib.config.gelin.count -= 30;
							game.saveConfig('gelin', lib.config.gelin);
							'step 1'
							game.over(true);
						}
					},
					'hundun_shunzhan': {
						trigger: {
							source: 'damageBegin4'
						},
						forced: true,
						filter: function(event, player) {
							return player.hp <= event.num;
						},
						content: function() {
							trigger.player.die();
						},
						ai: {
							jueqing: true
						},
						mod: {
							targetInRange: function(card, player, target, now) {
								if (card.name == 'sha') return true;
							},
							cardUsable: function(card, player, num) {
								if (card.name == 'sha') return Infinity;
							},
						},
					},
					'hundun_huxiao': {
						trigger: {
							player: 'phaseZhunbeiBegin',
						},
						content: function() {
							player.loseHp();
							player.addTempSkill('hundun_huxiao_draw');
						},
						subSkill: {
							draw: {
								trigger: {
									player: ['phaseDiscardBegin', 'phaseJudgeBegin']
								},
								charlotte: true,
								forced: true,
								content: function() {
									trigger.cancel();
									player.phaseDraw();
								}
							}
						}
					},
					'hundun_mingqi': {
						mod: {
							aiValue: function(player, card, num) {
								if (get.name(card) != 'tao' && get.color(card) == 'equip') return;
								var cards = player.getCards('hes', function(card) {
									return get.name(card) == 'tao' || get.color(card) != 'equip';
								});
								cards.sort(function(a, b) {
									return (get.name(a) == 'tao' ? 1 : 2) - (get.name(b) == 'tao' ? 1 : 2);
								});
								var geti = function() {
									if (cards.contains(card)) {
										return cards.indexOf(card);
									}
									return cards.length;
								};
								return Math.max(num, [6.5, 4, 3, 2][Math.min(geti(), 2)]);
							},
							aiUseful: function() {
								return lib.skill.kanpo.mod.aiValue.apply(this, arguments);
							},
						},
						locked: false,
						enable: 'chooseToUse',
						filterCard: function(card) {
							return get.color(card) != 'equip';
						},
						position: 'hes',
						viewAs: {
							name: 'tao'
						},
						prompt: '将一张牌当桃使用',
						check: function(card) {
							if (_status.event.player.hp > 0) return 0;
							return 15 - get.value(card);
						},
						ai: {
							threaten: 1.5,
						},
						group: 'hundun_mingqi_draw',
						subSkill: {
							draw: {
								trigger: {
									player: 'dyingAfter'
								},
								direct: true,
								content: function() {
									'step 0'
									player.draw(2);
									'step 1'
									player.chooseCard(2, '冥泣：将2张牌作为素材构筑4张必杀居合。').set('ai', function(card) {
										if (get.position(card) == 'e') return 1;
										if (get.type(card) == 'equip') return 2;
										return 15 - get.value(card);
									});
									'step 2'
									if (result.bool) {
										player.ly_gouzhu(['gl_bishajvhe', 'gl_bishajvhe', 'gl_bishajvhe', 'gl_bishajvhe'], result.cards);
									}
								}
							}
						}
					}
				},
				translate: {
					'hundun_wumianliming': '无冕黎明',
					'hundun_shunzhan': '瞬斩',
					'hundun_shunzhan_info': '锁定技，你使用【杀】没有次数与距离限制，当你造成X点及以上的伤害时，受到伤害的角色立即阵亡。（X为你当前的体力值）',
					'hundun_huxiao': '呼啸',
					'hundun_huxiao_info': '准备阶段开始时，你可以失去1点体力，令你本回合的判定阶段与弃牌阶段改为摸牌阶段。',
					'hundun_mingqi': '冥泣',
					'hundun_mingqi_info': '你可以将1张非装备牌当【桃】使用；当你脱离濒死状态后，你摸2张牌，随后可以将2张牌作为素材构筑4张【必杀居合】。',
				},
			},
			beyond: [{
				name: 'hundun_wumianliming',
				identity: 'zhu',
				position: 4
			}]
		}
	};
	lib.skinOL = {
		"hundun_wumianliming": "https://gitlab.com/nopower123/gelin/-/raw/main/online/hundun_wumianliming.jpg"
	}
}
