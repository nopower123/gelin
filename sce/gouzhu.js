/**
 * @description :
 * @author 尼斯湖水怪 date 2023/2/25
 */
window.initGouzhu = function(lib, game, ui, get, ai, _status) {
	//触发卡牌系统
	lib.skill.ly_triggerCard = {
		trigger: {},
		cards: {},
		direct: true,
		priority: 9,
		filter: function(event, player, name) {
			var cards = lib.skill.ly_triggerCard.cards;
			if (!cards[name]) return false;
			_status.ly_triggerCard = event;
			for (var i of cards[name]) {
				if (!player.hasUsableCard(i)) continue;
				if (lib.card[i].filter && !lib.card[i].filter(event, player, name)) continue;
				if (!game.hasPlayer(function(current) {
						return lib.filter.targetEnabled({
							name: i
						}, player, current)
					})) continue;
				for (var j in lib.card[i].trigger) {
					if (event[j] != player && j != 'global') continue;
					var trigger = lib.card[i].trigger[j];
					if (trigger == name || (Array.isArray(trigger) && trigger.contains(name))) {
						return true;
					}
				}
			}
			delete _status.ly_triggerCard;
			return false;
		},
		content: function() {
			'step 0'
			var cards = lib.skill.ly_triggerCard.cards;
			var next = player.chooseToUse(function(card, player) {
				var name = get.name(card);
				if (!cards[event.triggername].contains(name)) return false;
				if (!player.hasUsableCard(name)) return false;
				if (lib.card[name].filter && !lib.card[name].filter(trigger, player, event.triggername)) return false;
				for (var i in lib.card[name].trigger) {
					if (trigger[i] != player && i != 'global') continue;
					var tig = lib.card[name].trigger[i];
					if (tig == event.triggername || (Array.isArray(tig) && tig.contains(event.triggername))) {
						return lib.filter.cardEnabled(card, player, 'forceEnable');
					}
				}
				return false;
			});
			next.onresult = function(result) {
				result._apply_args = {
					_trigger: trigger,
				}
			}
			'step 1'
			if (result.bool && lib.skill.ly_triggerCard.filter(trigger, player, event.triggername)) {
				event.goto(0);
			}
		},
	};
	lib.translate.ly_triggerCard = "卡牌";
	if (!lib.ly_gouzhuList) lib.ly_gouzhuList = [];
	game.ly_triggerUp = function(pack) {
		game.removeGlobalSkill('ly_triggerCard');
		var info = lib.skill.ly_triggerCard;
		var trigger, card;
		for (var name of lib.cardPack[pack]) {
			card = lib.card[name];
			if (card.ly_material) lib.ly_gouzhuList.add(name);
			for (var i in card.trigger) {
				if (!info.trigger[i]) info.trigger[i] = [];
				if (Array.isArray(card.trigger[i])) {
					for (var j of card.trigger[i]) {
						trigger = card.trigger[i][j];
						info.trigger[i].add(trigger);
						if (!info.cards[trigger]) info.cards[trigger] = [];
						info.cards[trigger].add(name);
					}
				} else {
					trigger = card.trigger[i];
					info.trigger[i].add(trigger);
					if (!info.cards[trigger]) info.cards[trigger] = [];
					info.cards[trigger].add(name);
				}
			}
		}
		game.addGlobalSkill('ly_triggerCard');
	};
	if (!lib.element.card.inits) {
		lib.element.card.inits = [];
	}
	lib.element.card.inits.push(function(card) {
		if (!card.name) return;
		var info = lib.card[card.name];
		if (!info.ly_material) return;
		if (!card.node.ly_mp) card.node.ly_mp = ui.create.div('.ly_cardmp', card);
		card.node.ly_mp.innerHTML = '★' + (info.ly_mp || 0);
		console.log(card);
		console.log(info.ly_mp);
		console.log(card.node.range);
	});
	//配套魔力
	if (!lib.element.player.inits) {
		lib.element.player.inits = [];
	}
	lib.element.player.inits.add(function(player) {
		//初始化MP
		if (!player.node.ly_mp) {
			var mp = ui.create.div('.ly_mp', player);
			var div = ui.create.div(mp);
			player.node.ly_mp = ui.create.div(div);
			player.node.ly_textMp = document.createElement('span');
			div.appendChild(player.node.ly_textMp);
			ui.refresh(player.node.ly_mp);
		}
		player.ly_mp = parseInt(lib.ly_initMp || 0);
		player.ly_maxMp = parseInt(lib.ly_initMp || 0);
		var info = lib.character[player.name1];
		if (info) {
			info = info[4];
		} else {
			return;
		}
		for (var i of info) {
			if (i.indexOf('ly_mp:') == 0) {
				var mp = i.slice(6).split('/');
				player.ly_mp = parseInt(mp[0]);
				player.ly_maxMp = parseInt(mp[1] || mp[0]);
				break;
			}
		}
		player.ly_update();
	});
	lib.element.player.ly_update = function() {
		var num = (this.ly_mp / this.ly_maxMp) * 100;
		this.node.ly_textMp.innerHTML = this.ly_mp + '/' + this.ly_maxMp;
		this.node.ly_mp.style.width = num + '%';
	};
	lib.element.player.ly_changeMp = function(num) {
		var next = game.createEvent('ly_changeMp');
		next.player = this;
		next.num = num || 1;
		next.setContent('ly_changeMp');
		return next;
	};
	lib.element.content.ly_changeMp = function() {
		game.log(player, (num > 0 ? '回复' : '消耗') + '了' + Math.abs(num) + '点魔力');
		player.ly_mp += num;
		if (player.ly_mp > player.ly_maxMp) player.ly_mp = player.ly_maxMp;
		if (player.ly_mp < 0) player.ly_mp = 0;
		player.ly_update();
	};
	lib.element.player.ly_changeMaxMp = function(num) {
		if (num == 0) return;
		var next = game.createEvent('ly_changeMaxMp');
		next.player = this;
		next.num = num || 1;
		next.setContent('ly_changeMaxMp');
		return next;
	};
	lib.element.content.ly_changeMaxMp = function() {
		game.log(player, (num > 0 ? '获得' : '失去') + '了' + Math.abs(num) + '点魔力上限');
		player.ly_maxMp += num;
		if (player.ly_maxMp < 0) player.ly_maxMp = 0;
		player.ly_update();
	};
	lib.element.player.ly_isMaxMp = function(equal) {
		for (var i of game.players) {
			if (i.isOut() || i == this) continue;
			if (equal) {
				if (i.ly_mp >= this.ly_mp) return false;
			} else {
				if (i.ly_mp > this.ly_mp) return false;
			}
		}
		return true;
	};
	lib.element.player.ly_isMinMp = function(equal) {
		for (var i of game.players) {
			if (i.isOut() || i == this) continue;
			if (equal) {
				if (i.ly_mp <= this.ly_mp) return false;
			} else {
				if (i.ly_mp < this.ly_mp) return false;
			}
		}
		return true;
	};
	//构筑系统
	lib.element.player.ly_gouzhu = function(names, cards, mp) {
		var next = game.createEvent('lyGouzhu');
		next.player = this;
		if (Array.isArray(names)) {
			next.names = names.slice(0);
		} else if (typeof names == 'string') {
			next.names = [names];
		} else {
			next.names = ['sha'];
		}
		if (Array.isArray(cards)) {
			next.cards = cards.slice(0);
		} else if (get.itemtype(arguments[i]) == 'card') {
			next.cards = [cards];
		} else {
			next.cards = [];
		}
		next.mp = mp;
		next.setContent('ly_gouzhu');
		return next;
	}
	lib.element.content.ly_gouzhu = function() {
		'step 0'
		if (!event.mp) {
			event.mp = 0;
			for (var i of event.names) {
				event.mp += (lib.card[i].ly_mp || 0);
			}
		}
		player.ly_changeMp(-event.mp);
		player.lose(event.cards, ui.discardPile, 'visible').type = 'ly_gouzhu';
		game.log(player, '将', event.cards, '置入了弃牌堆');
		'step 1'
		event.resultCards = [];
		for (var i of event.names) {
			var card = game.createCard(i);
			event.resultCards.push(card);
			card.storage.ly_gouzhu = event.cards.slice(0);
		}
		if (game.me == player) player.$ly_gainCard(event.resultCards, event.cards);
		player.gain(event.resultCards);
	}
	if (!lib.ly_custom) lib.ly_custom = [];
	get.ly_gouzhuList = function(player) {
		var list = [];
		for (var func of lib.ly_custom) {
			func(player, list);
		}
		return game.checkMod(player, list, 'ly_gouzhuList', player);
	}
	lib.skill.ly_gouzhu = {
		enable: "phaseUse",
		chooseButton: {
			dialog: function(event, player) {
				var list = get.ly_gouzhuList(player);
				for (var i = 0; i < list.length; i++) {
					var name = list[i];
					list[i] = [get.translation(lib.card[name].type), '', name];
				}
				if (list.length == 0) {
					return ui.create.dialog('无可用牌库');
				}
				return ui.create.dialog('请选择需要构筑的卡牌', [list, 'vcard']);
			},
			select: function() {
				var player = _status.event.player;
				return game.checkMod(player, [1, 1], 'ly_selectGouzhu', player);
			},
			filter: function(button, player) {
				var info = lib.card[button.link[2]];
				var mp = (info.ly_mp || 0);
				for (var card of ui.selected.buttons) {
					mp += (lib.card[card.link[2]].ly_mp || 0);
				}
				if (player.ly_mp < mp) return false;
				for (var cost of info.ly_material) {
					if (player.countCards('hes', function(card) {
							return cost.filter(card, player, true);
						}) < get.select(cost.num)[0]) return false;
				}
				return true;
			},
			check: function(button) {
				var info = lib.card[button.link[2]];
				var player = _status.event.player;
				if (player.countCards('h', button.link[2]) > 0) return 0;
				var effect = player.getUseValue(button.link[2]);
				if (info.notarget) effect = info.ai.useful;
				if (effect > 0) return effect;
				return 0;
			},
			backup: function(links, player) {
				var cost = [];
				for (var i of links) {
					cost.addArray(lib.card[i[2]].ly_material);
				}
				return {
					filterOk: function() {
						for (var i of cost) {
							var num = 0;
							var select = get.select(i.num);
							for (var j of ui.selected.cards) {
								if (i.filter(j, _status.event.player)) num++;
							}
							if (select[0] == -1) {
								if (player.countCards('hes', function(card) {
										return cost.filter(card, _status.event.player);
									}) != num) return false;
							} else {
								if (select[0] > num) return false;
							}
						}
						return true;
					},
					filterCard: function(card, player, target) {
						for (var i of cost) {
							var num = 0;
							for (var j of ui.selected.cards) {
								if (i.filter(j, player)) num++;
							}
							var select = get.select(i.num)[1];
							if (select == -1) select = Infinity;
							if (select > num && i.filter(card, player)) return true;
						}
						return false;
					},
					check: function(card, player, target) {
						return 11 - get.value(card);
					},
					position: 'hes',
					selectCard: function() {
						var select = [-1, -1];
						for (var i of cost) {
							if (get.select(i.num)[0] > 0) select = [1, Infinity];
						}
						return select;
					},
					lose: false,
					links: links,
					delay: false,
					content: lib.skill.ly_gouzhu.contentx,
				}
			},
			prompt: function(links, player) {
				var names = [];
				for (var card of links) {
					names.push(card[2]);
				}
				return '请选择用于构筑的素材（' + get.translation(names) + '）';
			},
		},
		contentx: function() {
			var names = [];
			for (var card of lib.skill.ly_gouzhu_backup.links) {
				names.push(card[2]);
			}
			player.ly_gouzhu(names, event.cards);
		},
		ai: {
			order: 5,
			result: {
				player: 2,
			},
			threaten: 1.9,
		},
	};
	lib.translate.ly_gouzhu = "构筑";
	lib.translate.ly_gouzhu_backup = "构筑";
	game.addGlobalSkill('ly_gouzhu');
	//构筑特效
	lib.element.card.originalMoveDelete = function(player) {
		this.fixed = true;
		if (!this._listeningEnd || this._transitionEnded) {
			var dx, dy;
			if (this.classList.contains('center')) {
				var nx = [50, -52];
				var ny = [50, -52];
				nx = nx[0] * ui.arena.offsetWidth / 100 + nx[1];
				ny = ny[0] * ui.arena.offsetHeight / 100 + ny[1];
				dx = player.getLeft() + player.offsetWidth / 2 - 52 - nx;
				dy = player.getTop() + player.offsetHeight / 2 - 52 - ny;
			} else {
				this.style.left = this.offsetLeft + 'px';
				this.style.top = this.offsetTop + 'px';

				dx = player.getLeft() + player.offsetWidth / 2 - 52 - this.offsetLeft;
				dy = player.getTop() + player.offsetHeight / 2 - 52 - this.offsetTop;
			}
			if (get.is.mobileMe(player)) {
				dx += get.cardOffset();
				if (ui.arena.classList.contains('oblongcard')) {
					dy -= 16;
				}
			}
			if (this.style.transform && this.style.transform != 'none' && this.style.transform.indexOf('translate') == -1) {
				this.style.transform += ' translate(' + dx + 'px,' + dy + 'px)';
			} else {
				this.style.transform = 'translate(' + dx + 'px,' + dy + 'px)';
			}
			var that = this;
			setTimeout(function() {
				that.delete();
			}, 200);
		} else {
			this._onEndMoveDelete = player;
		}
	}
	lib.element.player.$ly_gainCard = function(card, cards) {
		game.pause();
		var card = card.slice(0);
		var cards = cards.slice(0);
		ui.arena.classList.add('playerfocus');
		var page = ui.create.div('.gl_gouzhu', ui.window);
		var cardNode = ui.create.div('.gl_fazhen');
		for (var i in lib.element.player) {
			cardNode[i] = lib.element.player[i];
		}
		ui.arena.appendChild(cardNode);
		var player = this;
		var boolCard = function() {
			var node = card.shift().copy('thrown', false);
			node.classList.add('playerfocus');
			node.style.transform = 'scale(0) rotateX(180deg)';
			node.style.left = 'calc(50% - 52px)';
			node.style.top = 'calc(50% - 52px)';
			setTimeout(function() {
				ui.arena.appendChild(node);
				ui.refresh(node);
				node.show();
				node.style.transform = '';
				setTimeout(function() {
					node.moveDelete(player);
				}, 600);
				if (card.length == 0) {
					setTimeout(function() {
						page.remove();
						cardNode.remove();
						game.resume();
						ui.arena.classList.remove('playerfocus');
					}, 600);
				} else {
					boolCard();
				}
			}, 500);
		}
		var bool = function() {
			if (cardNode.over) return;
			cardNode.over = true;
			boolCard();
		}
		if (!_status.connectMode) {
			var event = _status.event;
			event.forceMine = true;
			event.custom.replace.window = function() {
				if (!cardNode.over) {
					delete event.forceMine;
					bool();
					cardNode.over = true;
					game.resume();
				}
			}
		}
		var moveCard = function(node, num) {
			if (cardNode.over) return;
			var node;
			node = node.copy('thrown', false);
			node.classList.add('playerfocus');
			node.fixed = true;
			var top, left;
			switch (num % 4) {
				case 1:
					left = '100%';
					top = Math.random() * 100 + '%';
					break;
				case 2:
					left = '-10%';
					top = Math.random() * 100 + '%';
					break;
				case 3:
					left = Math.random() * 100 + '%';
					top = '100%';
					break;
				case 0:
					left = Math.random() * 100 + '%';
					top = '-10%';
					break;
			}
			node.style.left = left;
			node.style.top = top;
			node.style.transform = 'scale(0)';
			node.hide();
			ui.arena.appendChild(node);
			ui.refresh(node);
			node.show();
			node.style.transform = '';
			lib.listenEnd(node);
			setTimeout(function() {
				lib.element.card.originalMoveDelete.apply(node, [cardNode]);
				if (cards.length) {
					moveCard(cards.shift(), num + 1);
				} else {
					bool();
				}
			}, 500);
		}
		if (cards && cards.length) {
			setTimeout(function() {
				moveCard(cards.shift(), 1);
			}, 300)
		} else {
			bool();
		}
	}
}
